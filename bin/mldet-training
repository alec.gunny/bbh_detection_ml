#!/usr/bin/env python
# coding: utf-8

import os
import argparse
import logging

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

import mldet
from mldet import io, config, utils, net
from mldet.logger import logger, callbacks
    
# Default torch setting
torch.set_default_tensor_type(torch.FloatTensor)
    
# parse cmd argument
def parse_cmd():
    
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    
    # input/output/dataset settings
    parser.add_argument('--datadir')
    parser.add_argument('--dataset-name')
    parser.add_argument('--model-name')
    parser.add_argument('--ifo', default=config.DEFAULT_IFO)
    parser.add_argument('--log', type=str)
    
    # network settings
    parser.add_argument('--kernels', type=int, nargs='+', default=config.DEFAULT_KERNELS)
    parser.add_argument('--filters', type=int, nargs='+', default=config.DEFAULT_FILTERS)
    parser.add_argument('--dilations', type=int, nargs='+', default=config.DEFAULT_DILATIONS)
    parser.add_argument('--pooling', type=int, nargs='+', default=config.DEFAULT_POOLING)
    parser.add_argument('--linear', type=int, nargs='+', default=config.DEFAULT_LINEAR)
    parser.add_argument('--bn', type=io.str2bool, default=config.DEFAULT_BN)
    parser.add_argument('--dropout', type=float, default=config.DEFAULT_DROPOUT)
    parser.add_argument('--pooling-type', default=config.DEFAULT_POOLING_TYPE)
    parser.add_argument('--pooling-first', type=io.str2bool, default=config.DEFAULT_POOLING_FIRST)
    parser.add_argument('--input-shape', type=int, nargs='+')
        
    # training settings
    parser.add_argument('--batch-size', default=config.DEFAULT_BATCH_SIZE, type=int)
    parser.add_argument('--max-epochs', default=config.DEFAULT_MAX_EPOCHS, type=int)
    parser.add_argument('--num-workers', default=config.DEFAULT_NUM_WORKERS, type=int)
    parser.add_argument('--lr', default=config.DEFAULT_LR, type=float)
    parser.add_argument('--weight-decay', default=config.DEFAULT_WEIGHT_DECAY, type=float)
    parser.add_argument('--partition', type=int, nargs='+', default=config.DEFAULT_PARTITION)
    parser.add_argument('--seed', default=None, type=int)
    
    # cuda settings
    parser.add_argument('--device', default=config.DEFAULT_DEVICE, type=str)
    
    params = parser.parse_args()
    return params

params = parse_cmd()
params_dict = vars(params)

# logging
logging.basicConfig(filename=params.log, filemode='a', 
                    format='%(asctime)s - %(message)s', level=logging.DEBUG)
logging.info('Dataset dir: {}'.format(params.datadir))
logging.info('Dataset name: {}'.format(params.dataset_name))
logging.info('Network name: {}'.format(params.model_name))

# Use GPU if available
device = mldet.utils.get_device(params.device)
params_dict['device'] = device

# Create network model, loss function, and optimizer
model = net.get_model(**params_dict)
criterion = nn.BCEWithLogitsLoss(reduction='sum')
optimizer = optim.Adam(model.parameters(), lr=params.lr,
                       weight_decay=params.weight_decay)
# scheduler = mldet.scheduler.get_scheduler(params_dict, optimizer)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 10, 0.1)
# scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
#     optimizer, factor=0.1, patience=10, threshold=
# )

# Create logger and callback function
binhist = callbacks.binhist.BinHist(labels=('Noise', 'Signal'))
cmatrix = callbacks.cmatrix.CMatrix(labels=('Noise', 'Signal'))
log = logger.ClassifierLogger(
    model_name=params.model_name, data_name=params.dataset_name, 
    metrics=['loss'])

# Ceate dataloader
train_dataset, val_dataset, _, _ = utils.get_datasets(
    params.datadir, shuffle=True, ifo=params.ifo,
    seed=params.seed, partition=params.partition, 
)
train_loader = DataLoader(
    train_dataset, batch_size=params.batch_size, shuffle=True, 
    pin_memory=True, num_workers=params.num_workers)
val_loader = DataLoader(
    val_dataset, batch_size=params.batch_size, shuffle=True, 
    pin_memory=True, num_workers=params.num_workers)

# Start training
utils.print_train()
num_batches = len(train_loader)
for epoch in range(params.max_epochs):

    # Training 
    model.train()
    train_loss = 0.
    for i_batch, (x_batch, y_batch) in enumerate(train_loader):
        optimizer.zero_grad()  # reset gradient

        # move batch to GPU
        x_batch = x_batch.to(device)
        y_batch = y_batch.to(device)
    
        # forward pass
        yhat_batch = model(x_batch)
        
        # calculate loss, backward pass and gradient descent
        loss = criterion(yhat_batch, y_batch)
        loss.backward()
        optimizer.step()
        
        # update training loss
        train_loss += loss.item()
        
    # Update learning rate if LR scheduler is given
    if scheduler is not None:
        scheduler.step()
        
    # Evaluating at the end of every epoch
    model.eval()
    # reset callbacks function
    binhist.reset()
    cmatrix.reset()
    val_loss = 0.
    with torch.no_grad():
        for i_batch, (x_batch, y_batch) in enumerate(val_loader):
            # move batch to GPU
            x_batch = x_batch.to(device)
            y_batch = y_batch.to(device)
            
            # forward pass
            yhat_batch = model(x_batch)
            pred_batch = yhat_batch.round()
            
            # calculate loss
            loss = criterion(yhat_batch, y_batch)
            
            # update validation loss
            val_loss += loss.item()
    
            # update callbacks
            binhist.update(yhat_batch, y_batch)
            cmatrix.update(pred_batch, y_batch)

    # Get average loss per sample
    train_loss /= len(train_loader.dataset)
    val_loss /= len(val_loader.dataset)
            
    # Logging at the end of epoch
    # update metric
    log.update_metric(
        train_loss, val_loss, 'loss', epoch, num_batches, num_batches)
    
    # update score histogram and confusion matrix
    log.save_binhist(binhist, epoch, num_batches)
    log.save_cmatrix(cmatrix, epoch, num_batches)
    
    # save metric and model
    log.log_metric()
    log.save_model(model, epoch)
    log.save_optimizer(optimizer, epoch)

    # print out status
    log.display_status(
        train_loss, val_loss, 'loss', epoch, params.max_epochs, i_batch, num_batches)
