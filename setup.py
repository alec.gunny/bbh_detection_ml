#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pkg_resources
from setuptools import setup, find_packages

__version__ = '0.0.0'
    
setup(
    name='mldet',
    version=__version__,
    author='''Tri Nguyen''',
    author_email='tri.nguyen@ligo.org',
    maintainer='Tri Nguyen',
    maintainer_email='tri.nguyen@ligo.org',
    description='',
    packages=find_packages(),
    classifiers=(
      'Programming Language :: Python',
      'Development Status :: 3 - Alpha',
      'Intended Audience :: Science/Research',
      'Intended Audience :: End Users/Desktop',
      'Intended Audience :: Developers',
      'Natural Language :: English',
      'Topic :: Scientific/Engineering',
      'Topic :: Scientific/Engineering :: Astronomy',
      'Topic :: Scientific/Engineering :: Physics',
      'Operating System :: POSIX',
      'Operating System :: Unix',
      'Operating System :: MacOS',
      'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    ),
    scripts=(
        'bin/mldet-training',
        'bin/mldet-testing',
        'bin/mldet-real-event',
    ),
    python_requires='>=3.5',
)

