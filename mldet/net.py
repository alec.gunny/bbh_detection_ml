
import logging

import torch
import torch.nn as nn

logger = logging.getLogger(__name__)

INIT_WITH_GAIN =(
    nn.init.orthogonal_, 
    nn.init.xavier_uniform_, 
    nn.init.xavier_normal_
)

def get_model(**params):
    ''' Convenient function to get network model '''
    input_shape = params['input_shape']
    checkpoint = params.get('checkpoint')
    model = Net(input_shape, params).to(params['device'])
    if checkpoint is not None:
        logging.info(f'- Loading checkpoint: {checkpoint}')
        model.load_state_dict(torch.load(checkpoint, map_location=params['device']))    
    logging.info(model)
    return model
    
class Flatten(nn.Module):
    def __init__(self):
        super().__init__()
    
    def forward(self, x):
        return x.view(x.size(0), -1)

    
class Net(nn.Module):
    def __init__(self, input_shape, params):
        super().__init__()

        self.params = params
        
        # featurizer
        featurizer = []
        prev_f  = input_shape[0]
        for i in range(len(params['filters'])):
            # get current kernel and filter
            curr_f = params['filters'][i]
            curr_k = params['kernels'][i]
            curr_p = params['pooling'][i]
            curr_d = params['dilations'][i]
            
            # add Module to list
            featurizer.append(nn.Conv1d(prev_f, curr_f, curr_k, dilation=curr_d))
            
            if params['pooling_type'].lower() == 'max':
                if curr_p > 1:
                    pool_layer = nn.MaxPool1d(curr_p, curr_p)
                else:
                    pool_layer = None
            elif params['pooling_type'].lower() == 'avg':
                if curr_p > 1:
                    pool_layer = nn.AvgPool1d(curr_p, curr_p)
                else:
                    None        
            act_layer = nn.ReLU(inplace=True)
            if params['pooling_first']:
                if pool_layer is not None: 
                    featurizer.append(pool_layer)
                featurizer.append(act_layer)
            else:
                featurizer.append(act_layer)
                if pool_layer is not None: 
                    featurizer.append(pool_layer)
            # update filter
            prev_f = curr_f
        self.featurizer = nn.Sequential(*featurizer)
        
        # flatten
        self.flatten = Flatten()
    
        # classifier
        classifier = []
        prev_d = self._get_conv_shape(input_shape)
        use_bias = not params['bn']
        for i in range(len(params['linear'])):
            curr_d = params['linear'][i]
            classifier.append(nn.Linear(prev_d, curr_d, bias=use_bias))
            classifier.append(nn.ReLU(inplace=True))
            classifier.append(nn.Dropout(params['dropout']))
            if params['bn']:
                classifier.append(nn.BatchNorm1d(curr_d))
            prev_d = curr_d
        classifier.append(nn.Linear(curr_d, 1))
#         classifier.append(nn.Sigmoid())
        self.classifier = nn.Sequential(*classifier)
        
        # initializer weight
        self.gain = nn.init.calculate_gain('relu')
        self.init(params.get('weight_init'), params.get('bias_init'))
    
    def _get_conv_shape(self, input_shape):
        x = torch.ones((1, *input_shape))
        x = self.flatten(self.featurizer(x))
        return x.size(1)
    
    def init(self, weight_init, bias_init):
        def fun(layer):
            if type(layer) in (nn.Linear, nn.Conv1d):
                # weight init
                if weight_init in INIT_WITH_GAIN:
                    weight_init(layer.weight, self.gain)
                elif weight_init:
                    weight_init(layer.weight)
                
                # bias init
                if bias_init and layer.bias is not None:
                    if bias_init in init_with_gain:
                        bias_init(layer.bias, self.gain)
                    else:
                        bias_init(layer.bias)
        self.apply(fun)
    
    def forward(self, x):
        x = self.featurizer(x)
        x = self.flatten(x)
        x = self.classifier(x)
        return x
    