
import numpy as np


# def get_true_pos(signal_scores, threshold):
#     ''' Get true positive given threshold '''
    
#     for thres in threshold:
#         tp.append(np.sum(signal_scores >= thres) / len(signal_scores))
    
    
# def get_false_pos(noise_scores, threshold):
#     ''' Get false positive given threshold '''
#         tp.append(np.sum(signal_scores >= thres) / len(signal_scores))

    

def getROC(signal_scores, noise_scores, threshold):
    ''' Get true positive and false positive'''
    true_pos, false_pos = [], []
    
    for thres in threshold:
        tp = np.sum(signal_scores >= thres) / len(signal_scores)
        fp = np.sum(noise_scores >= thres) / len(noise_scores)
        true_pos.append(tp)
        false_pos.append(fp)
    true_pos = np.array(true_pos)
    false_pos = np.array(false_pos)
    
    return true_pos, false_pos