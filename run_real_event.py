
import os
import argparse
import subprocess

from mldet import io
from mldet.config import REAL_EVENT_PARAMS_KEYS

# Parse command line argument
def parse_cmd():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    parser.add_argument('config', help='Path to config file', type=str) 
    params = parser.parse_args()
    return params

params = parse_cmd()
config = io.parse_config(params.config, 'config')

# Call training script
real_event_cmd = 'mldet-real-event ' + io.dict2args(config, REAL_EVENT_PARAMS_KEYS)
print('Run cmd: ' + real_event_cmd)
print('--------------------------')
subprocess.check_call(real_event_cmd.split(' '))
